<?php
	define('OVERLAY', true);
	include('templates/header.php');
?>

	<main class="KLayout">
		<section id="swagIntro" class="heroDisplay overlay">
			<article>
				<img src="content/neon-logo.svg" style="width: 100px; height: 100px; margin: 0px auto; display: block;" />
				<h1>Introducing KDE neon</h1>
				<p>
					The latest and greatest of KDE community software.
				</p>
				<br>
				<p align="center">
					<a href="https://neon.kde.org/download" id="neonInstallButton">Download Now</a>
				</p>
				<div class="d-flex flex-wrap">
					<aside class="kSocialLinks homeSocialLinks">News<br />
					    
						<a class="shareNeon" target="_blank" href="https://blog.neon.kde.org/" aria-label="KDE neon blog"></a>
						<a class="shareFacebook" target="_blank" href="https://facebook.com/kdeneon" aria-label="Share on Facebook"></a>
						<a class="shareMastodon" target="_blank" href="https://social.opendesktop.org/@kde_neon" aria-label="Share on Mastodon"></a></li>
					</aside>
					<aside class="kSocialLinks text-right ml-auto homeSocialLinks">
					Support<br />
						<a class="shareForum" target="_blank" href="https://discuss.kde.org/tag/neon" aria-label="KDE Discuss"></a></li>
						<a class="shareFacebook" target="_blank" href="https://www.facebook.com/groups/931803210238672" aria-label="KDE neon users Facebook group"></a>
						<a class="shareTelegram" target="_blank" href="https://telegram.me/kdeneon" aria-label="Telegram group"></a></li>
						<a class="shareMatrix" target="_blank" href="https://webchat.kde.org/#/room/#kde-neon-users:kde.org" aria-label="Matrix room"></a></li>
					</aside>
				</div>
			</article>
		</section>

		<section>
				<img src="content/home/laptop2.png" class="splashImage" />
				<article>
					<h1>Straight From KDE</h1>
					<p>More than ever, people expect cutting-edge features
					delivered right from their developers, with no modifications
					or gatekeeping. KDE neon delivers this by packaging the
					hottest software fresh from the KDE Community ovens, the
					moment it’s released.</p>

					<p>You should use KDE neon if you are an adventurous KDE
					enthusiast who wants the latest and greatest from the KDE
					community as soon as it’s available, with no delays,
					opinionated patches, or UX changes. This is KDE software
					the way KDE designed it!</p>
				</article>

				<article>
					<h1>Make Computing Your Own with Plasma Desktop</h1>
					<p>
						We think that your desktop is YOUR desktop. Make
						it unique with the option to dive into every
						minimal detail from visuals to work patterns.
					</p>
					<p>
						Plasma Desktop from the KDE community is a smart,
						effective, and beautiful environment from the
						very first time you start it. Using KDE neon,
						Plasma and KDE applications will be continuously updated, so no more waiting,
						adding package archives or downloading source code
						if you want what’s new.
					</p>

				</article>
            <article>
                <h1 id="snap">Shells</h1>
                <p>
                  <a href="https://www.shells.com/pricing?special=kde&_a=kdeorg&utm_source=web&utm_medium=lp&utm_campaign=neon-kde-org"><img src="images/shells-green.png" class="splashImage" /></a>
                  Get a powerful, secure desktop that you can take anywhere. Access your Shells on multiple devices from a smart TV to your smartphone. You can even breathe fresh life into that old computer. Make your next computer a computer in the cloud with Shells.
                </p>
                <p>
                  Transform any device into a powerful, secure desktop with Shells.  Whether you want to code on your smartphone or produce a fresh new track on your TV, with Shells you can unlock the full performance and experience of a desktop computer on any device.
                </p>
                <div class="downloadButton">
                    <a href="https://www.shells.com/pricing?special=kde&_a=kdeorg&utm_source=web&utm_medium=lp&utm_campaign=neon-kde-org">Get your Plasma on Shells</a>
                </div>
                <p style="font-size: smaller">KDE earns a small commission when you use this link.</p>
                <p>
                  <a href="https://www.shells.com/pricing?special=kde&_a=kdeorg&utm_source=web&utm_medium=lp&utm_campaign=neon-kde-org"><img src="images/shells-neon.png" width="600" class="splashImage" /></a><br />
                  <em>A Shells desktop running KDE neon in a web browser</em>
                </p>
            </article>
		</section>
	</main>

<?php

	include ('templates/footer.php');
?>
