<?php
/**
 * Available variables for the header:
 * $headerCssFiles = [];
 * $headerJsFiles = [];
 * $headerBodyClasses = '';
 */
    $headerCssFiles = ['content/download.css'];
    $headerBodyClasses = '';
    include('templates/header.php');
    include('lib/IsoDates.php');
    $isoDates = new IsoDates();
    $isoDates->init();
?>
<main class="KLayout">
    <section class="container" id="KNeonArticle">
        <div class="row text-center">
            <article>
                <h1 id="live">Live Images</h1>
                <p>
                    Live images are ideal for people who want a clean installation. Use a live image to replace your existing system, install alongside existing operating systems, or simply try KDE neon without affecting your computer.
                </p>
                <p>
                    <strong>If you intend to use KDE neon in a VirtualBox VM, please turn on 3D acceleration in the machine settings.</strong>
                </p>
            </article>

            <div class="col-12 col-md-6 vertical-border" >
                <img src="content/download/userMedia.svg" class="splashImage" />
                <h3>User Edition<sup class="tag">64-bit</sup></h3>
                <p class="text-center">
                    Featuring the latest officially released KDE software on a stable base. Ideal for adventurous KDE enthusiasts.
                </p>
                <div class="downloadButton">
                    <a href="https://files.kde.org/neon/images/user/<?php print $isoDates->user ?>/neon-user-<?php print $isoDates->user ?>.iso">User Edition Live/Install Image</a>
                </div>
                <br />
                <a href="https://files.kde.org/neon/images/user/<?php print $isoDates->user ?>/neon-user-<?php print $isoDates->user ?>.iso.sig">PGP signature for verification</a>
                <br />
                <a href="https://files.kde.org/neon/images/user/<?php print $isoDates->user ?>/neon-user-<?php print $isoDates->user ?>.iso.torrent">Torrent file</a>
            </div>

            <div class="col-12 col-md-6 vertical-border" >
                <img src="content/download/developerMedia.svg" class="splashImage" />
                <h3>Testing Edition<sup class="tag">64-bit</sup></h3>
                <p class="text-center">
                    Featuring pre-release KDE software built the same day from bugfix branches. Good for testing. There is no QA. Will contain bugs.
                </p>
                <div class="downloadButton">
                    <a href="https://files.kde.org/neon/images/testing/<?php print $isoDates->testing ?>/neon-testing-<?php print $isoDates->testing ?>.iso">Testing Edition Live/Install Image</a>
                </div>
                <br />
                <a href="https://files.kde.org/neon/images/testing/<?php print $isoDates->testing ?>/neon-testing-<?php print $isoDates->testing ?>.iso.sig">PGP signature for verification</a>
                <br />
                <a href="https://files.kde.org/neon/images/testing/<?php print $isoDates->testing ?>/neon-testing-<?php print $isoDates->testing ?>.iso.torrent">Torrent file</a>

                <h3>Unstable Edition<sup class="tag">64-bit</sup></h3>
                <p class="text-center">
                    Featuring pre-release KDE software built the same day from new feature branches. Good for testing. There is no QA. Will contain many bugs.
                </p>
                <div class="downloadButton">
                    <a href="https://files.kde.org/neon/images/unstable/<?php print $isoDates->unstable ?>/neon-unstable-<?php print $isoDates->unstable ?>.iso">Unstable Edition Live/Install Image</a>
                </div>
                <br />
                <a href="https://files.kde.org/neon/images/unstable/<?php print $isoDates->unstable ?>/neon-unstable-<?php print $isoDates->unstable ?>.iso.sig">PGP signature for verification</a>
                <br />
                <a href="https://files.kde.org/neon/images/unstable/<?php print $isoDates->unstable ?>/neon-unstable-<?php print $isoDates->unstable ?>.iso.torrent">Torrent file</a>

                <h3>Developer Edition<sup class="tag">64-bit</sup></h3>
                <p class="text-center">
                    Unstable Editon plus development libraries pre-installed.
                </p>
                <div class="downloadButton">
                    <a href="https://files.kde.org/neon/images/developer/<?php print $isoDates->developer ?>/neon-unstable-developer-<?php print $isoDates->developer ?>.iso">Developer Edition Live/Install Image</a>
                </div>
                <br />
                <a href="https://files.kde.org/neon/images/developer/<?php print $isoDates->developer ?>/neon-unstable-developer-<?php print $isoDates->developer ?>.iso.torrent">Torrent file</a>
            </div>
<!--
            <article>
                <div class="text-center">
                    <a href="http://srcfiles.neon.kde.org/" target="_blank" style="color: #909c9c;">Download Source ISOs</a>
                </div>
            </article>
-->
            <article>
                <p>Install using KDE ISO Image Writer.</p>
                <p>Download the ISO file, use KDE ISO Image Writer (or on Mac we recommend ROSA Image Writer) to make a bootable USB drive with the ISO file and reboot using that USB drive.
                <ul>
                    <li> <a href="https://apps.kde.org/isoimagewriter/" class="internal">KDE ISO Image Writer</a>
                    </li>
                    <li> <a href="http://wiki.rosalab.ru/ru/images/3/33/RosaImageWriter-2.6.2-osx.dmg" class="internal" title="RosaImageWriter-2.6.2-osx.dmg">ROSA Image Writer for Mac OS X</a>
                    </li>
                </ul>
                <p>GPG signatures signed by <a href="https://keyserver.ubuntu.com/pks/lookup?op=get&search=0x45F4C354638D1F29">KDE
                neon ISO Signing Key</a> (0x45F4C354638D1F29) are available alongside the ISOs for verification.</p>
            </article>

            <article>
                <h1 id="shells">Shells</h1>
                <p>
                  <a href="https://www.shells.com/pricing?special=kde&_a=kdeorg&utm_source=web&utm_medium=lp&utm_campaign=neon-kde-org"><img src="images/shells-green.png" class="splashImage" /></a>
                  Get a powerful, secure desktop that you can take anywhere. Access your Shells on multiple devices from a smart TV to your smartphone. You can even breathe fresh life into that old computer. Make your next computer a computer in the cloud with Shells.
                </p>
                <p>
                  Transform any device into a powerful, secure desktop with Shells.  Whether you want to code on your smartphone or produce a fresh new track on your TV, with Shells you can unlock the full performance and experience of a desktop computer on any device.
                </p>
                <div class="downloadButton">
                    <a href="https://www.shells.com/pricing?special=kde&_a=kdeorg&utm_source=web&utm_medium=lp&utm_campaign=neon-kde-org">Get your Plasma on Shells</a>
                </div>
                <p style="font-size: smaller">KDE earns a small commission when you use this link.</p>
                <p>
                  <a href="https://www.shells.com/pricing?special=kde&_a=kdeorg&utm_source=web&utm_medium=lp&utm_campaign=neon-kde-org"><img src="images/shells-neon.png" width="600" class="splashImage" /></a><br />
                  <em>A Shells desktop running KDE neon in a web browser</em>
                </p>
            </article>

            <article>
                <h1 id="live">KDE neon in Containers</h1>
                <p>
                    <a href="https://community.kde.org/Neon/Containers"><img src="images/containers.jpg" class="splashImage" width="400" /></a>
                    Containers are great for testing or developing on KDE software without the need to re-install your operating system or a virtual machine. We build ours daily and you can use <b>distrobox</b> to install it.  They run with <b>docker</b> and <b>podman</b> and other container managers.
                    <div class="downloadButton">
                        <a href="https://community.kde.org/Neon/Containers">Container Images Documentation</a>
                    </div>
                </p>
            </article>

            <article>
                <h1 id="snap">Snap Packages</h1>
                <p>
                    <a href="https://snapcraft.io/publisher/kde"><img src="https://snapcraft.io/static/images/badges/en/snap-store-black.svg" class="splashImage" /></a>
                    Most KDE apps now are available as Snap packages which can be installed on any Linux distro. These are made using KDE neon technology.
                </p>
                <p>
                    Snap comes ready to use on KDE neon.
                </p>
                <p>
                    For other distros you may need to <a href='https://snapcraft.io/docs/snap-tutorials'>set up Snap on your system</a>, you can then install packages from the Store or through Plasma Discover.
                </p>
                <div class="downloadButton">
                    <a href="https://snapcraft.io/publisher/kde">KDE on the Snap Store</a>
                </div>
            </article>

            <!--
            <article>
                <div class="text-left">
                    <p>Special Case Editions:</p>
                    <ul>
                        <li><a href="https://files.kde.org/neon/images/ko/current/">Korean Testing Edition Live/Install Image</a> pre-configured with Korean locale.</li>
                        <li><a href="https://files.kde.org/neon/images/pinebook-remix-nonfree/">Pinebook Remix Images</a> build with some non-free drivers for the ARM based Pinebook laptop.</li>
                    </ul>
                </div>
            </article>
            -->
        </div>
    </section>
</main>

<?php
    include ('templates/footer.php');
