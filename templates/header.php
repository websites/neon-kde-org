<!DOCTYPE html>
<html class="pt-0">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="author" content="KDE Neon team">
	<meta name="description" content="KDE neon is the latest and greatest of KDE community software.">
    <meta name="keywords" content="KDE, neon, community, Ubuntu, LTS">	
	<title>KDE neon</title>
	<!-- IE up to 9, excluding 10, ignoring this rubbish. -->
	<!--[if IE]><link href="media/favicon.ico" rel="shortcut icon" /><![endif]-->
	<!-- IE11/Chrome/Opera/Safari -->
	<link href="media/192x192.png" rel="icon" sizes="192x192" />
	<!-- iOS -->
	<link href="media/180x180.png" rel="apple-touch-icon" sizes="180x180" />
	<!-- Sane Browsers -->
	<link href="media/128x128.svg" rel="icon" sizes="128x128" />
	<link href="media/64x64.svg" rel="icon" sizes="64x64" />
	<link href="media/32x32.svg" rel="icon" sizes="32x32" />
	<link href="media/24x24.svg" rel="icon" sizes="24x24" />
	<link href="media/16x16.svg" rel="icon" sizes="16x16" />
	<link href="https://cdn.kde.org/aether-devel/bootstrap.css" rel="stylesheet" type="text/css" />
	<script src="https://cdn.kde.org/aether-devel/bootstrap.js" defer></script>
	<link href="/css/neon-layout.css?ver=1" rel="stylesheet" type="text/css" />
	<?php
		if (isset($headerCssFiles))
			foreach($headerCssFiles as $file)
				echo "\n\t".'<link href="'.$file.'" rel="stylesheet" type="text/css" />';

		if (isset($headerScriptFiles))
			foreach($headerScriptFiles as $file)
				echo "\n\t".'<script src="'.$file.'"></script>';
	?>
</head>
<body class="KStandardSiteStyle">
	<header id="KGlobalHeader" class="header clearfix"> 
		<nav id="" class="navbar navbar-expand-lg align-items-center" >
			<a href="index" class="brand-logo" id="KNeonLogo">
			<img src="content/neon-logo.svg" alt="KDE Neon Logo" class="KNeonLogo" />KDE neon</a>
			
			<button class="navbar-toggler navbar-toggler-right align-items-center" type="button" data-toggle="collapse" data-target="#KNeonNavbar" aria-controls="KNeonNavbar" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			
			<div class="collapse navbar-collapse flex-row-reverse" id="KNeonNavbar">
				<ul class="navbar-nav">
					<li class="nav-item">
						<a class="nav-link" href="/faq">FAQ</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="/develop">Developer Edition</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="https://kde.slimbook.es" class="button">KDE Slimbook</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="/download" class="button">Download KDE neon</a>
					</li>
				</ul>
			</div>
		</nav>
     </header>
