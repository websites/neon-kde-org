	<footer id="kFooter" class="footer">
		<section id="kFooterIncome" class="container">
			<div id="kDonateForm">
				<div class="center">
				<h2 class="h5 mt-2 mb-3">Donate to KDE <a href="https://kde.org/community/donations/index.php#money" target="_blank">Why Donate?</a></h2>
				<form action="https://www.paypal.com/en_US/cgi-bin/webscr" method="post" onsubmit="return amount.value>=2||window.confirm(&quot;Your donation is smaller than 2€. This means that most of your donation\nwill end up in processing fees. Do you want to continue?&quot;);"><input type="hidden" name="cmd" value="_donations">
					<input type="hidden" name="lc" value="GB">
					<input type="hidden" name="item_name" value="Development and communication of KDE software">
					<input type="hidden" name="custom" value="//kde.org/index/donation_box">
					<input type="hidden" name="currency_code" value="EUR">
					<input type="hidden" name="cbt" value="Return to kde.org">
					<input type="hidden" name="return" value="https://kde.org/community/donations/thanks_paypal">
					<input type="hidden" name="notify_url" value="https://kde.org/community/donations/notify.php">
					<input type="hidden" name="business" value="kde-ev-paypal@kde.org">
					<label class="sr-only" for="donateAmountField">Amount</label><input type="text" name="amount" value="20.00" id="donateAmountField" data-kpxc-id="donateAmountField"> €
					<button type="submit" id="donateSubmit">Donate via PayPal</button>
				</form>
				<a class="mt-3 d-inline-block h6" href="https://kde.org/community/donations/others" target="_blank">Other ways to donate</a>
				</div>
			</div>
			<div id="kMetaStore">
				<div class="center">
				<h2 class="h5 mb-3 mt-2">Visit the KDE MetaStore</h2>
				<p class="h6">Show your love for KDE! Purchase books, mugs, apparel, and more to support KDE.</p>
				<a href="https://kde.org/stuff/metastore" class="button">Click here to browse</a>
				</div>
			</div>
		</section>
		<section id="kLinks" class="container pb-4">
			<div class="row">
				<nav class="col-sm">
				<h3>Products</h3>
				<a href="https://kde.org/plasma-desktop" hreflang="">Plasma</a>
				<a href="https://apps.kde.org/" hreflang="">KDE applications</a>
				<a href="https://develop.kde.org/products/frameworks/">KDE Frameworks</a>
				<a href="https://plasma-mobile.org/overview/">Plasma Mobile</a>
				<a href="https://neon.kde.org/">KDE Neon</a>
				</nav>
				<nav class="col-sm">
				<h3>Develop</h3>
				<a href="https://techbase.kde.org/">Techbase Wiki</a>
				<a href="https://api.kde.org/">API Documentation</a>
				<a href="https://doc.qt.io/" rel="noopener" target="_blank">Qt Documentation</a>
				<a href="https://inqlude.org/" rel="noopener" target="_blank">Inqlude Documentation</a>
				<a href="https://kde.org/goals">KDE Goals</a>
				<a href="https://invent.kde.org/">Source Code</a>
				</nav>
				<nav class="col-sm">
				<h3>News &amp; Press</h3>
				<a href="https://kde.org/announcements/">Announcements</a>
				<a href="https://dot.kde.org/">KDE.news</a>
				<a href="https://planet.kde.org/">Planet KDE</a>
				<a href="https://kde.org/screenshots">Screenshots</a>
				<a href="https://kde.org/contact/">Press Contacts</a>
				<a href="https://kde.org/stuff">Miscellaneous Stuff</a>
				<a href="https://kde.org/thanks">Thanks</a>
				</nav>
				<nav class="col-sm">
				<h3>Resources</h3>
				<a href="https://community.kde.org/Main_Page">Community Wiki</a>
				<a href="https://userbase.kde.org/">UserBase Wiki</a>
				<a href="https://kde.org/support/">Support</a>
				<a href="https://kde.org/download/">Download KDE Software</a>
				<a href="https://kde.org/code-of-conduct/">Code of conduct</a>
				<a href="https://kde.org/privacypolicy">Privacy Policy</a>
				<a href="https://kde.org/privacypolicy-apps">Applications Privacy Policy</a>
				</nav>
				<nav class="col-sm">
				<h3>Destinations</h3>
				<a href="https://store.kde.org/">KDE Store</a>
				<a href="https://ev.kde.org/">KDE e.V.</a>
				<a href="https://kde.org/community/whatiskde/kdefreeqtfoundation">KDE Free Qt Foundation</a>
				<a href="https://timeline.kde.org/">KDE Timeline</a>
				<a href="https://manifesto.kde.org/">KDE Manifesto</a>
				<a href="https://kde.org/support/international/">International Websites</a>
				</nav>
			</div>
		</section>
		<div id="kSocial" class="container kSocialLinks"><a class="shareMatrix" href="https://webchat.kde.org/#/rooms/#kde:kde.org" rel="nofollow">Share on Matrix</a>
			<a class="shareFacebook" href="https://www.facebook.com/kde/" rel="nofollow">Post on Facebook</a>
			<a class="shareTwitter" href="https://twitter.com/kdecommunity" rel="nofollow">Share on Twitter</a>
			<a class="shareTheads" href="https://www.threads.net/@kdecommunity" rel="nofollow">Share on Threads</a>
			<a class="shareMastodon" href="https://floss.social/@kde" rel="me nofollow">Share on Mastodon</a>
			<a class="shareLinkedIn" href="https://www.linkedin.com/company/29561/" rel="nofollow">Share on Reddit</a>
			<a class="shareReddit" href="https://www.reddit.com/r/kde/" rel="nofollow">Share on Reddit</a>
			<a class="shareYouTube" href="https://www.youtube.com/channel/UCF3I1gf7GcbmAb0mR6vxkZQ" rel="nofollow">Share on YouTube</a>
			<a class="sharePeerTube" href="https://tube.kockatoo.org/a/kde_community/video-channels" rel="nofollow">Share on Peertube</a>
			<a class="shareVK" href="https://vk.com/kde_ru" rel="nofollow">Share on VK</a>
			<a class="shareInstagram" href="https://www.instagram.com/kdecommunity/" rel="nofollow">Share on Instagram</a>
		</div>
		<div id="kLegal" class="container">
			<div class="d-flex flex-wrap mb-0 py-3 h6"><small lang="en">Maintained by <a href="mailto:kde-www@kde.org">KDE Webmasters</a> (public mailing list).</small>
				<small class="ml-auto text-right">KDE<sup>®</sup> and <a href="https://kde.org/media/images/trademark_kde_gear_black_logo.png">the K Desktop Environment<sup>®</sup> logo</a> are registered trademarks of <a href="https://ev.kde.org/" title="Homepage of the KDE non-profit Organization">KDE e.V.</a> | <a href="https://www.kde.org/community/whatiskde/impressum">Legal</a></small>
			</div>
		</div>
	</footer>	
</body>
</html>
